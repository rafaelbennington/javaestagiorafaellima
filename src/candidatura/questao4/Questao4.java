
package candidatura.questao4;
import java.util.Random;
import java.util.Scanner;
/**
 *
 * @author Rafael
 */
public class Questao4 {
    private String mes;
    static Scanner tecla = new Scanner(System.in);
    Random random = new Random();
    private int m = random.nextInt(12);
    
    public String getMes(){
        return mes;
    }
    public void setMes(String mes){
        this.mes = mes;
    }
    
    private void diaMes(int m){
            switch(m){
                case 1: System.out.println("Mês com 31 dias"); break;
                case 2: System.out.println("Mês com menos de 30 dias"); break;
                case 3: System.out.println("Mês com 31 dias"); break;
                case 4: System.out.println("Mês com 30 dias"); break;
                case 5: System.out.println("Mês com 31 dias"); break;
                case 6: System.out.println("Mês com 30 dias"); break;
                case 7: System.out.println("Mês com 31 dias"); break;
                case 8: System.out.println("Mês com 31 dias");break;
                case 9: System.out.println("Mês com 30 dias"); break;
                case 10: System.out.println("Mês com 31 dias"); break;
                case 11: System.out.println("Mês com 30 dias"); break;
                case 12: System.out.println("Mês com 31 dias"); break;             
                case 0: break; 
        }
    }
    protected void qualDiaMes(){
        if (m < 1){
            m += 1;
        }
        System.out.println("\nQuestao 4.1");
        System.out.println("\nQuestao 4.2");
        System.out.println("Mês: " + m);
        diaMes(m);
    }
    public void print(){
        qualDiaMes();
    }
}
