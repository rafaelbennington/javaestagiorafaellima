/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package candidatura.questao9;

/**
 *
 * @author Rafael
 */
public class Pessoa implements Comparable<Pessoa>{
	
	private static int cont = 0;

	//ATRIBUTOS
	private int id;
	private int idade;
	private String nome;
	
	//MÉTODOS DE ACESSO
	public int getId() {
		return id;
	}
	
	public void setId() {
		this.id = id;
	}
	
	public int getIdade() {
		return idade;
	}
	
	public void setIdade() {
		this.idade = idade;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome() {
		this.nome = nome;
	}
	
	//CONSTRUTOR
	public Pessoa(int id, int idade, String nome) {
		
		this.id = id;
		this.idade = idade;
		this.nome = nome;
		
	}

	@Override
	public int compareTo(Pessoa outraPessoa) {
		return(this.idade - outraPessoa.getIdade());
		/*if(this.idade < outraPessoa.getIdade()) {
			return -1;
		}
		else if(this.idade > outraPessoa.getIdade()) {
			return 1;
		}
		else{
			return 0;*/
		
	}
	/*public int compareTo1(Pessoa p) {
		return nome.compareTo(p.getNome());
	}*/
		
}
