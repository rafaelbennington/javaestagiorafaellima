
package javaestagiorafaellima;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collections;
import candidatura.questao1.Questao1;
import candidatura.questao2.Questao2;
import candidatura.questao3.Questao3;
import candidatura.questao4.Questao4;
import candidatura.questao7.A;
import candidatura.questao7.B;
import candidatura.questao7.C;
import candidatura.questao9.CachePessoa;
import candidatura.questao9.Pessoa;

/**
 *
 * @author Rafael
 */
public class JavaEstagioRafaelLima {
    static Scanner tecla = new Scanner(System.in);
    static int indice = 0;
    static ArrayList<Pessoa> lista = new ArrayList<>();
    static ArrayList<CachePessoa> cache = new ArrayList<>(); 
    static Scanner input = new Scanner(System.in); 

    public static void main(String[] args) {
            Questao1 questao1 = new Questao1();
            questao1.imprimir();
            
            Questao2 questao2 = new Questao2();
            questao2.imprimir();
            
            Questao3 questao3 = new Questao3();
            questao3.imprimir();
            
            Questao4 questao4 = new Questao4();
            questao4.print();
           System.out.println("\nQuestão 7"); 
           System.out.println("Digite a classe: A, B ou C");
           String l = tecla.next();
           
           executar(l);
           
           System.out.println("\nQuestao 9");
           int opcao;
		do {
			System.out.println("=-=-=-=MENU PRINCIPAL=-=-=-=");
			System.out.println(" 1 - CADASTRAR PESSOA ");
			System.out.println(" 2 - CONSULTAR UMA PESSOA ");
			System.out.println(" 3 - CONSULTAR TODAS AS PESSOAS");
			System.out.println(" 4 - SAIR DA APLICAÇÃO ");
			opcao = input.nextInt();
			switch(opcao) {
				case 1: cadastrarPessoa(); break;
				case 2: consultarUmaPessoa(); break;
				case 3: consultarTodasPessoa(); break;
				case 4: System.out.println("Programa encerrado!"); break;
			}
		}while (opcao!=4);
            }
    public static void executar(String l){
        A a = new A();
        B b = new B() {};
        C c = new C();
        if (l.equalsIgnoreCase("A")){
            a.implementeMe(l);
        }
        if (l.equalsIgnoreCase("B")){
            b.implementeMe(l);
        }
        if (l.equalsIgnoreCase("C")){
            c.implementeMe(l);
        }
    }
  	public static void cadastrarPessoa() {
		System.out.println("Digite o primeiro nome da Pessoa: ");
		String nome = input.next();
		System.out.println("Digite a idade da Pessoa: ");
		int idade = input.nextInt();
		System.out.println("Digite o ID da pessoa: ");
		int id = input.nextInt();
		lista.add(new Pessoa(id, idade, nome));
		//cache.add(new CachePessoa(0, 0, null));

		System.out.println("Pessoa cadastrada com sucesso!!!");
	}
	public static void consultarUmaPessoa() {
		int verifica = 0; //CASO VERIFICA FOR = 0, IRÁ ADCIONAR A PESSOA CONSULTADA NA CACHE
		System.out.println("Digite o ID: ");
		int ide = input.nextInt();
		for(CachePessoa c : cache) {
			if (c.getId() == ide) {
				c.retornaPessoa(ide);
				System.out.println("Essa pessoa já foi consultada anteriormente!!!\n");
				verifica = 1;
				break;
			}
		}
		if(verifica == 0) {
			for(Pessoa p : lista) {
				if(ide == p.getId()) {
					System.out.println("Nome: "+p.getNome()+"\nIdade: "+
							p.getIdade()+"\nIdentificador: "+p.getId());
					cache.add(new CachePessoa(p.getId(), p.getIdade(), p.getNome()));
					break;
				}			
			}
			
		}		
	}
	public static void consultarTodasPessoa() {
		Collections.sort(lista);
		int total = 0;
		for (int i = 0; i< lista.size(); i++) {
			System.out.println("Nome: " +lista.get(i).getNome()+"; Idade: " +lista.get(i).getIdade() +
					"; Identificador: " + lista.get(i).getId());
			total ++;
			
		}
		System.out.println("Total de pessoas cadastradas: " +total);
	}
	

}
